/*
 *    Copyright 2019 Lucas Ces Santos
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package main

import (
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/lucas.ces/notification-operator/controllers"
	"gitlab.com/lucas.ces/notification-operator/core"
	"gitlab.com/lucas.ces/notification-operator/event_processors"
	"gitlab.com/lucas.ces/notification-operator/operators"
	"net/http"
	"os"
)

func main() {
	config := parseConfig()
	listenerController := controllers.ListenerController{}
	pubSub := core.ServerlessPubSub{}
	websocketOperator := operators.WebsocketPubSub{}
	logEventProcessor := event_processors.LogEventProcessor{}
	dispatcherEventProcessor := event_processors.DispatcherEventProcessor{PubSub: pubSub}
	proxyEventProcessor := event_processors.ProxyEventProcessor{Processors: []core.EventProcessor{dispatcherEventProcessor, logEventProcessor}}

	websocketOperator.Init(pubSub)
	listenerController.Init(proxyEventProcessor)
	startHttpServer(config)
	listenerController.Close()

}

func parseConfig() core.Config {
	var port = os.Getenv("HTTP_PORT")
	if len(port) == 0 {
		port = "13133"
	}

	return core.Config{HttpPort: port}
}

func addrBuilder(port string) *string {
	return flag.String("addr", "0.0.0.0:"+port, "http service address")
}

func startHttpServer(config core.Config) {
	var addr = addrBuilder(config.HttpPort)
	flag.Parse()
	log.Info(fmt.Sprintf("Starting http server at port: %s", config.HttpPort))
	log.Fatal(http.ListenAndServe(*addr, nil))
}
