/*
 *    Copyright 2019 Lucas Ces Santos
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package event_processors

import (
	"bytes"
	"encoding/json"
	"gitlab.com/lucas.ces/notification-operator/core"
	"html/template"
	"reflect"
)

type DispatcherEventProcessor struct {
	PubSub core.PubSub
}

func (d DispatcherEventProcessor) ProcessEvent(data []byte) {
	dest := core.Destination{Provider: "slack", Destinations: []string{"devnull"}}
	message := "{{ .involvedObject.name }} has been started into {{ .involvedObject.namespace }}"
	tmpl, err := template.New("test").Funcs(tf).Parse(message)
	if err != nil {
		println("error")
		return
	}
	var tmp bytes.Buffer
	var obj map[string]interface{}
	err = json.Unmarshal(data, &obj)
	if err != nil {
		println("error")
		return
	}
	err = tmpl.Execute(&tmp, obj)
	if err != nil {
		println("error")
		return
	}
	msg := core.PubSubMessage{Destination: dest, Data: tmp.String()}
	me := core.MatchExpression{Key: "reason", Operator: "in", Values: []string{"Started"}}
	if me.Evaluate(data) {
		d.PubSub.Publish(msg)
	}
}

// https://gist.github.com/alex-leonhardt/8ed3f78545706d89d466434fb6870023
// https://www.calhoun.io/intro-to-templates-p3-functions/
// https://blog.golang.org/laws-of-reflection
// https://golang.org/pkg/reflect/
// https://golang.org/pkg/text/template/#FuncMap
var tf = template.FuncMap{
	"isInt": func(i interface{}) bool {
		v := reflect.ValueOf(i)
		switch v.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint32, reflect.Uint64, reflect.Float32, reflect.Float64:
			return true
		default:
			return false
		}
	},
	"isString": func(i interface{}) bool {
		v := reflect.ValueOf(i)
		switch v.Kind() {
		case reflect.String:
			return true
		default:
			return false
		}
	},
	"isSlice": func(i interface{}) bool {
		v := reflect.ValueOf(i)
		switch v.Kind() {
		case reflect.Slice:
			return true
		default:
			return false
		}
	},
	"isArray": func(i interface{}) bool {
		v := reflect.ValueOf(i)
		switch v.Kind() {
		case reflect.Array:
			return true
		default:
			return false
		}
	},
	"isMap": func(i interface{}) bool {
		v := reflect.ValueOf(i)
		switch v.Kind() {
		case reflect.Map:
			return true
		default:
			return false
		}
	},
}
