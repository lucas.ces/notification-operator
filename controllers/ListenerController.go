/*
 *    Copyright 2019 Lucas Ces Santos
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package controllers

import (
	"container/list"
	"gitlab.com/lucas.ces/notification-operator/core"
	"gitlab.com/lucas.ces/notification-operator/listeners/checks"
	"gitlab.com/lucas.ces/notification-operator/listeners/events"
	"gitlab.com/lucas.ces/notification-operator/listeners/kubernetes"
)

var listeners = list.New()
var stops = list.New()

func init() {
	registerListener(checks.LivenessListener{})
	registerListener(checks.ReadinessListener{})
	registerListener(events.WebsocketListener{})
	registerListener(kubernetes.EventListener{})
}

func registerListener(listener core.EventListener) {
	listeners.PushBack(listener)
}

type ListenerController struct {
}

func (ListenerController) Init(processor core.EventProcessor) {

	for l := listeners.Front(); l != nil; l = l.Next() {
		stop := l.Value.(core.EventListener).Init(processor)
		if stop != nil {
			stops.PushFront(stop)
		}
	}
}

func (l ListenerController) Close() {
	for stops.Len() > 0 {
		stop := stops.Back()
		close(stop.Value.(chan struct{}))
	}
}
