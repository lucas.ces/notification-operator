/*
 *    Copyright 2019 Lucas Ces Santos
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package operators

import (
	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/lucas.ces/notification-operator/core"
	"net/http"
)

var upgrader = websocket.Upgrader{} // use default options

type WebsocketPubSub struct {
}

func (l WebsocketPubSub) Init(pubSub core.PubSub) {
	log.Info("Initializing operators back-channel")
	bc := pubSub.Subscribe()
	http.HandleFunc("/ws-operators", func(w http.ResponseWriter, r *http.Request) {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
		c, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Print("upgrade:", err)
			return
		}
		defer c.Close()
		mt, _, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
		}
		for {
			message := <-bc

			err = c.WriteMessage(mt, []byte(message.Data))
			if err != nil {
				log.Println("write:", err)
				break
			}
		}
	})
}
